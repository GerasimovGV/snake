# README #

Простая игра типа "Змейка" созданый для обучения JS/HTML5/CSS

Задействованные технологии: 

 * JS
    * ООП
    * Динамические массивы
    * Связанные списки, и связанная с ними РЕКУРСИЯ
    * Вызов методов с помощью func.call(self, ...);
    * Реакция на нажатия клавиш
    * Вызов функций "подписчиков" на события (для взаимодействия с HTML не смешивая с кодом объектов Змейки)
    * Работа с таймером

 * HTML5
    * Canvas
    * output
    * progress

 * CSS
    * display: grid

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
когда-нибудь это будет игра Змейка.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact