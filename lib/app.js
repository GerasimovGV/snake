var GameArea;//объект canvas
var ctx;//Контекст canvas
var tmrMove = null;//идентификатор таймера передвижения
var Snake = null;
var Mills = null;
var foneColor = "rgb(101, 214, 218)";//цвет фона
var score;//это элемент на html куда я вывожу счёт игры (кол-во сегментов в змее)
var progress;//прогрес бар на html который показывает заполненность желудка

function newGame(){
    drawGameArea();
    Snake = null;
    Mills = null;
    if (tmrMove != null) clearInterval(tmrMove);//остановить таймер
    Snake = new TSnake(drawGameArea());
        //добавляю подписчика
        Snake.changeScore = changeScore;//на добавление сегмента
        Snake.ateMill = snakeAteMill;//на добавление еды в желудок
    Mills = new TMills(Snake.settings);
    Snake.mill = Mills;
    tmrMove = setInterval(onTimer, 200);
}

window.onload = function() {
    console.log('Game started' );
    //получение контекста для рисования
    GameArea = document.getElementById('GameArea'),
    ctx = GameArea.getContext('2d');
    newGame();//создать поле игры и объекты (змею и пищу)
    score =  document.getElementById('score');
    progress = document.getElementById('progress');
};

//Подписчики
//changeScore - подписка на добавление сегмента к змее
//В segmentsNumber передаётся кол-во сегментов в змее (-1)
function changeScore(segmentsNumber) {
    score.value = segmentsNumber;
}

//snakeAteMill - подписка на событие проглатывания пищи
//В MillQuantity передаётся заполненность желудка змеи
function snakeAteMill(MillQuantity) {
    progress.value = MillQuantity;
}

//вывод надписи Game Over по центру экрана контента
function printGameOver(){
    ctx.fillStyle = "Black";
    ctx.strokeStyle = "White";
    ctx.font = 'bold 40px sans-serif';
    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.lineWidth = 1;
    ctx.strokeText("Game Over", GameArea.width / 2, GameArea.height /2);
}

var onTimeCnt = 0;
function onTimer(){
    if (Snake != null) {
        Snake.move();
        Snake.testMill();
        Snake.testBody();
        Mills.draw();
        if (Snake.fault) {
            clearInterval(tmrMove);//остановить таймер
            printGameOver();//вывести надпись Game Over
        }
    };
    console.info('onTimer:', onTimeCnt++ );
}

//drawGameArea рисует клетчатое поле 10х10 клеток на canvas
function drawGameArea(){
    //нам требуетсяы 10 линий (гориз и вертикальных)
    //исходя из ширины widht рассчитаем шаг смещения по х и y
    var cells = 20;//сколько требуется клеток
    var stepV = GameArea.width  / cells;//приращение коорд Х для вертикальных линий
    var stepH = GameArea.height / cells;//приращение коорд Y для горизонтальных линий
    ctx.lineWidth = 0.01;//ширина линии
    ctx.beginPath();
        ctx.fillStyle = foneColor;//цвет фона из CSS
        ctx.fillRect(0,
                        0, 
                            GameArea.width,
                                GameArea.height);
        var i=0;
        var x, y;    
        while (i != cells) {
            i++;
            x = i*stepV;
            y = i*stepH;
            //рисуем вертикальные линии  
            ctx.moveTo(x,0);
            ctx.lineTo(x,GameArea.height);
            //рисуем горизонтальные линии
            ctx.moveTo(0,y);
            ctx.lineTo(GameArea.width, y);
        };      
    ctx.stroke();
    return settings = {
       cells : cells,
       stepV : stepV,  
       stepH : stepH
    }
}

//добавить голову змее (например, при поглощении пищи)
function addHead() {
    console.log('addHail');
    if (Snake != null) {
        Snake.addHead();
    }
}

//обработка клавиатуры
document.onkeydown = function (e) {
    if (Snake != null) {
        switch (e.key) {
            case 'ArrowUp':
                Snake.setDirection('Up');
            break;
        case 'ArrowDown':
                Snake.setDirection('Down');
             break;
        case 'ArrowLeft':
                Snake.setDirection('Left');
            break;
        case 'ArrowRight':
                Snake.setDirection('Right');
            break;
        }
    }
};
////////////////////////////////////////////////////////////////////
//TMills - еда(корм)
//TMill  - кусочки еды
    function TMill(settings){
        this.state =  100;//при создании. кусочек = 100%
        ///////////////////////////////////////////////
        this.xCell = 10;//x координата ячейки текущие
        this.yCell = 10;//y координата ячейки текущие
    }

    function TMills(settings){
        this.a = [];//массив корма
        this.settings = settings;
        
        this.setRandomCell = function(m){
            var x = Math.floor(Math.random() * this.settings.cells);
            var y = Math.floor(Math.random() * this.settings.cells);
            m.xCell = x;
            m.yCell = y;
        }
        //на поле всегда должно быть 1-3 кусочка еды.
        //Сканирую список и уменьшаю state каждого кусочка
        //Как только state одного из кусочков стал = 0 (кусочек исчез)
        //то присоднияю следующий за ним кусок к предыдущему, и удалаю этот кусок с нулём еды
        this.checkMill = function(){
            console.log('TMills.checkMill');
            var i;
            var m;
            if (this.a.length ==0 ) {//начальная ситуация
                i = Math.floor(Math.random() * 3)+1;
                while (i--) {
                    m = new TMill(this.settings);//создал кусок еды
                    this.setRandomCell(m);//выдал случайные координаты (за исключением тех где змея)
                    this.a.push(m);
                }
            }
            else {
                i = this.a.length;
                while (i) {
                    i--;
                    m = this.a[i];
                    //уменьшаю state
                    if (m.state) {//если есть что уменьшать - уменьшаю
                        if (--m.state == 0){//если доуменьшался до нуля, то 
                            //удаляю объект из массива
                            //и пере определяю массив
                            this.a.splice(i,1);
                        }
                    }
                }
            }
        }

        this.addMill = function(){

        }


        //отрисовка кусочка пищи
        this.drawPcOfMill = function(m){
            console.log('TMills.drawPcOfMill','x:',m.xCell, ' y:',m.yCell);
            var x = m.xCell * this.settings.stepV;
            var y = m.yCell * this.settings.stepH;
            ctx.beginPath();
                //восстановлю фон по предыдущим координатам
                ctx.fillStyle = foneColor;//цвет фона из CSS
                ctx.fillRect(x,
                                y, 
                                    this.settings.stepV,
                                        this.settings.stepH);
                //вычисляю новое состояние еды, это шарик красного цвета
                //который уменьшается с каждой прорисовкой на state >1
                //1) координаты центра
                var rx = x + (this.settings.stepV /2);
                var ry = y + (this.settings.stepH /2);;
                var r = (this.settings.stepV * (m.state / 100))/2;
                ///////////////////////
                ctx.fillStyle = "red";                                   
                ctx.arc(rx,ry,//координаты центра
                            r, //радиус
                                0,//начальный угол
                                    0.15,//конечный угол
                                        true);
            ctx.fill();//закрашенная область            
        }

        //проходит по списку кусочков и отображает их один за другим
        this.draw = function(){
            this.checkMill();
            var i = this.a.length;
            while (i) {
                i--;
                this.drawPcOfMill(this.a[i]);
            }
        }

    }

////////////////////////////////////////////////////////////////////
//TSnake - змея
    //Segment - часть змея (элемент связанного списка)
    function TSegment(settings) {
        //Когда змей состоит из одной клетки, то его pred = nill
        //  Голова:
        //      pred = предыдущий сегмент
        //  Хвост:
        //      pred = null ;//нет ничего после хвоста
        this.pred = null;//ссылка на предыдущий сегмент (ближе к хвосту)
        ///////////////////////////////////////////////
        this.xCell = 10;//x координата ячейки текущие
        this.yCell = 10;//y координата ячейки текущие
        this.xpCell = 10;//x координата ячейки предыдущие
        this.ypCell = 10;//y координата ячейки предыдущие
    }

    //TSnake - Змей
    function TSnake(settings) {
        this.fault = false;//true если змей куснул своё тело
        var segment = new TSegment();
        this.settings = settings;
        this._head = segment;//голова змея
        this._segmentsNumber = 0;//кол-во сегментов
        this.direction = 'Up';
        this.mill = null;//объект TMills с едой
        this.stomach = 0;//желудок (как только заполняеься до 100%, у змеи добавляется сегмент)
        //подписки
        this.changeScore = null;//если не нуль до вызывается при добавлении сегмента
        this.ateMill = null;//если не нуль до вызывается при поедании пищи

        //устанавливает направление Up/Down/Left/Right
        this.setDirection = function (direction) {
            this.direction = direction;
            console.log('TSnake.setDirection:', direction);
        }

        this.getNextXY = function (h){
            var x = h.xCell;
            var y = h.yCell;
            switch (this.direction) {
                case 'Up'://попытка шага вверх
                    if (--y < 0) //если ушёл в минус, значит появится с другой стороны поля
                        y = this.settings.cells - 1;//координаты ячейки от 0 до cells-1
                    break;
                case 'Down'://попытка шага ввниз
                    if (++y == this.settings.cells) //если ушёл за границы  поля, значит появится с другой стороны поля
                        y = 0;//координаты ячейки
                    break;
                case 'Left'://попытка шага вверх
                    if (--x < 0) //если ушёл в минус, значит появится с другой стороны поля
                        x = this.settings.cells - 1;//координаты ячейки от 0 до cells-1 
                    break;
                case 'Right'://попытка шага ввниз
                    if (++x == this.settings.cells) //если ушёл за границы  поля, значит появится с другой стороны поля
                        x = 0;//координаты ячейки
                break;
            }
            return {x, y};
        }
        
        //узнаёт куда направлена голова
        //вычисляет координаты перд ней, и устанавливает новые координаты в новую голову
        this.getNewHeadProperty = function(newHead){
            console.log('TSnake.getNewHeadProperty');
            //смотрит Direction на основнии чего изменяет координаты головы
            var p = this.getNextXY(this._head);
            //устанавливаю новые отекущие и предыдущие координаты
            newHead.xpCell = newHead.xCell = p.x;
            newHead.ypCell = newHead.yCell = p.y;
        }

        //добавляет голову;
        //голова должна добавляться в ту сторону, куда направляется змея
        this.addHead = function (){
            var h = new TSegment(this.settings);//создал новый сегмент
            this.getNewHeadProperty(h);//придал ему новые координаты (впереди текущей головы)
            //новой голове говорю что у неё есть шея
            h.pred = this._head;
            //и теперь новую голову делаю основной головой
            this._head = h;
            this._segmentsNumber++;//сегментов стало больше 
            //оповещение подписчиков
            if (this.changeScore != null)
                this.changeScore(this._segmentsNumber);
        }

        //рисует сегмент змея
        this.drawSeg = function (Seg){
            console.log('TSnake.drawSeg');
            var border = 0;
            console.log('TSnake.draw ','x:',Seg.xCell, ' y:',Seg.yCell);
            var x = Seg.xCell * this.settings.stepV;
            var y = Seg.yCell * this.settings.stepH;
            ctx.beginPath();
                //восстановлю фон по предыдущим координатам
                ctx.fillStyle = foneColor;//цвет фона из CSS
                ctx.fillRect(Seg.xpCell * this.settings.stepV+border,
                                Seg.ypCell * this.settings.stepV+border, 
                                    this.settings.stepV-border,
                                        this.settings.stepH-border);
                //уравниваю предыдущие координаты с новыми
                Seg.xpCell = Seg.xCell;
                Seg.ypCell = Seg.yCell;  
                //нарисую новое положение
                ctx.fillStyle = "black";                                   
                ctx.fillRect(x+border,y+border, 
                        this.settings.stepV-border,
                            this.settings.stepH-border);
            ctx.stroke();
        }

        //рисует змея
        //при этом в x,y переданы новые, корректные координаты головы (вычесленные в move)
        this.draw = function (x,y){
            //запоминаю ссылку на объект self и адрес функции drawSeg
            //по другому иначе как func.call(Self,...) метод объекта не вызвать из функции
            //которая к объекту не относится
            var self = this;
            var func = this.drawSeg;
            console.log('TSnake.draw');
            (function recurse(Seg, x, y) {
                Seg.xCell = x;
                Seg.yCell = y;
                x = Seg.xpCell;
                y = Seg.ypCell;
                func.call(self, Seg);//способ вызвать this.drawSeg(Seg, ...);
                if (Seg.pred != null) {//если у сегмента есть сосед ближе к хвосту, то
                    recurse(Seg.pred, x, y);//запускаю рекурсию
                }
            })(this._head, x, y);
        }
        //передвигает змея на одну позицию, а по сути управляет направлением головы
        //вызывается из таймера. Один тик - один мув
        this.move = function () {
            console.log('TSnake.move');
            //смотрит Direction на основнии чего изменяет координаты головы
            var p = this.getNextXY(this._head);
            this.draw(p.x, p.y);
        }

        //сравниваю координать головы и еды;
        this.testMill = function (){
            console.log('TSnake.testMill');
            var x = this._head.xCell;
            var y = this._head.yCell;
            var i = this.mill.a.length;
            var m;
            while (i){
                i--;
                m = this.mill.a[i];
                if ((x == m.xCell) && (y == m.yCell)) {//если координаты еды и головы совпали
                    this.stomach +=m.state;
                    //проверка на заполненность желудка и если заполнен то добавление сегмента
                    if (this.stomach >= 100) {
                        this.addHead();//добавить голову
                        this.stomach = 0;//очистить желудок
                    }
                    this.mill.a.splice(i,1);//удалить еду;
                    //вызвать подписчика, чтобы показать наполненность желудка
                    if (this.ateMill != null)
                        this.ateMill(this.stomach);
                }
            }
        }

        //проверяет столкновение головы с телом.
        //если куснул себя то Game Over
        this.testBody = function(){
            console.log('TSnake.testBody');
            //координаты головы
            var x = this._head.xCell;
            var y = this._head.yCell;
            try {
                (function recurse(p) {//в рекурсию запускаю предудущий элемент
                    if (p != null) {
                        if ((x == p.xCell) && (y == p.yCell)) {//если координаты сегмента и головы совпали
                            throw "Game Over";//надо выйти из рекурсии с сигналом GameOver
                        }
                        else {//голова не куснула сегмент
                            recurse(p.pred);
                        }
                    }
                })(this._head.pred);
            }
            catch (e){
                console.log(e);
                this.fault = true;
                return;
            }
        }
    }